  $(document).ready(function(){
      var xhr=null;
      var lm=0;
      var chat_enabled=false;
      var myInterval;
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function chat_load(){
  $.ajax({
    type:"post",
    url:"/commun_chat_show",
    datatype: "json",
    data:{
      id:1,
      lm:lm
    },
    success:function(data){
      if(lm==0){
        var reversed = data.reverse();
      }
      if(data['length']>0){
        for(var i in data){
          var date=data[i]['date'].split(" ");
          if(data[i]['profileAvatar']==null) data[i]['profileAvatar']='/images/default.jpg';
          if(data[i]['id_user']==$('.session_user').attr('id')){
            $('#area_chat').append("<div class='mini_avatar ml-auto d-table' style=\"background:url('"+data[i]['profileAvatar']+"')no-repeat scroll center center / cover;\"></div><li class='msg text-right mr-4' style='align-self:flex-end'>"+data[i]['message']+"</li><small class='text-right mr-4 text-primary'>"+date[1]+"</small>");
          }else{
            $('#area_chat').append("<div class='mini_avatar d-table' style=\"background:url('"+data[i]['profileAvatar']+"')no-repeat scroll center center / cover;\"></div><a href='/profile/"+data[i]['id_user']+"'><small>"+data[i]['name']+"</small></a><li class='msg ml-4'>"+data[i]['message']+"</li><small class='ml-4 text-primary'>"+date[1]+"</small>");
          }
        }
        lm=data[i]['id'];
        var elem = document.getElementById('area_chat');
        elem.scrollTop = elem.scrollHeight;
      }
      }
    });
  };
function sendMessage(e){
  e.preventDefault();
  var val=$('#textarea_message').val();
  $('#textarea_message').val('');
  $.ajax({
    type:"post",
    url:"/chat_send",
    data:{
      message:val,
      chat:1
    },
    success:function(){
      chat_load();
      }
  });
};
$('#chat_close').click(function(){
clearInterval(myInterval);
myInterval=false;
chat_enabled=false;
lm=0;
});
$('#chat_toggler').click(function(){
  if(chat_enabled==false){
    chat_enabled=true;
    $.ajax({
      type:"post",
      url:"/commun_chat_check",
      success:function(data){
        if(data=='Join'){
          $('#area_typing').css('display','none');
          $('#area_join').click(function(){
            $.ajax({
              type:"post",
              url:"/commun_chat_join",
              success:function(data){
                $('#area_chat').html('');
                chat_load();
                myInterval=setInterval(chat_load,3000);
                $('#area_typing').css('display','block');
                $('#chat_form').on('submit',sendMessage);
              }
            })
          })
        }else if(data=="Welcome"){
          $('#area_chat').html('');
          chat_load();
          myInterval=setInterval(chat_load,3000);
          $('#area_typing').css('display','block');
          $('#chat_form').on('submit',sendMessage);
          }
        }
    });
  }
});
});