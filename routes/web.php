<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::user()!=null){
        return redirect('/profile');
    }
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@showProfile');
Route::get('/profile/{id}', 'ProfileController@showProfileFriend');
Route::post('/profile_image_update', 'ProfileController@avatarUpdate');
Route::post('/profile_image_delete', 'ProfileController@avatarDelete');
Route::post('/user_register','Auth\RegisterController@register');
Route::post('/user_login','Auth\LoginController@login');
Route::get('/logout',function(){
    Auth::logout();
    return redirect('/');
});
// Moderation
Route::get('/moderation','ProfileController@showModerPage');
// Administation 
Route::get('/admin','ProfileController@showAdminPage');
Route::post('/num_of','AdminController@Counts');
Route::post('/admin_show','AdminController@AdminBackOfficeShow');
// Moderation actions
Route::post('/add_content','AdminController@ContentAdd');
Route::post('/content_get','AdminController@ContentGet');
Route::post('/change_content','AdminController@ContentChange');
// Les Cours
Route::get('/cours','PageController@showLessons');
Route::get('/cours/{id}','PageController@showLesson');
Route::post('/lesson_complete','ProfileController@completeLesson');
//Les posts
Route::get('/posts','PageController@showPosts');
//Language change
Route::get('/{id}','ProfileController@LangChange');
//Chat
Route::post('/commun_chat_check','ProfileController@CommunChatCheck');
Route::post('/commun_chat_show','ProfileController@ShowChat');
Route::post('/commun_chat_join','ProfileController@CommunChatJoin');
Route::post('/user_avatar','ProfileController@userAvatar');
Route::post('/user_name','ProfileController@userName');
Route::post('/chat_send','ProfileController@sendMessageChat');