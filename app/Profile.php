<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Profile extends Model
{
    protected function getUser($id){
        return DB::table('users')
        ->where('id',$id)
        ->first();
    }
}
