<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
class AdminController extends Controller
{
    public function Counts(Request $request){
        if($request->num_of=='all'){
            return DB::table('lessons')
            ->select('id')
            ->get();
        }
        else if($request->num_of=='bunkei'||$request->num_of=='kaiwa'||$request->num_of=='reibun'){
            return DB::table('lessons_'.$request->num_of)
            ->groupBy('id_lesson')
            ->select('id_lesson')
            ->get();
        }
        else if($request->num_of=='bunkei_francais'||$request->num_of=='kaiwa_francais'||$request->num_of=='reibun_francais'){
            return DB::table($request->num_of)
            ->groupBy('id_lesson')
            ->select('id_lesson')
            ->get();
        }
        else if($request->num_of=='new_words'||$request->num_of=='lesson_info'){
            return DB::table('fr_'.$request->num_of)
            ->groupBy('id_lesson')
            ->select('id_lesson')
            ->get();
        }
    }
    public function ContentAdd(Request $req){
        $object=$req->object;
        $lesson=$req->lesson;
        $content=$req->content;
        $head=$req->head;
        if($req->object=="bunkei"||$req->object=="reibun"){
             DB::insert("insert into lessons_$object (id_lesson,content) values (?,?)",[$lesson,$content]);
             $this->checkLesson($lesson);
        }
        else if($object=="kaiwa"){
             DB::insert("insert into lessons_$object (id_lesson,head,content) values (?,?,?)",[$lesson,$head,$content]);
             $this->checkLesson($lesson);
        }
        else if($object=="kaiwa_francais"){
             DB::insert("insert into $object (id_lesson,head,content) values (?,?,?)",[$lesson,$head,$content]);
        }
        else if($object=="bunkei_francais"||$object=="reibun_francais"){
             DB::insert("insert into $object (id_lesson,content) values (?,?)",[$lesson,$content]);
        }
        else if($object=="lesson_info"){
             DB::insert("insert into fr_$object (id_lesson,head,content) values (?,?,?)",[$lesson,$head,$content]);
        }
        else if($object=="new_words"){
             DB::insert("insert into fr_$object (id_lesson,words) values (?,?)",[$lesson,$content]);
        }
        else if($object=="post"){
            $title=$req->title;
            $id_post=DB::table('user_post')
            ->orderBy('id','desc')
            ->first();
             DB::insert("insert into user_post (date,title,content,id_author) values (?,?,?,?)",[date("Y-m-d H:i:s"),$title,$req->text,Auth::user()->id]);
             if ($req->hasFile('photo')) {
                $img = Image::make($_FILES['photo']['tmp_name']);
                $img->fit(1280, 720);
                $img->save('uploads/post/'.(++$id_post->id).".jpeg");
             }
             return 'Fait!';
        }
        return "Fait!";
    }
    public function ContentGet(Request $req){
        $content=$req->content;
        $content_of=$req->content_of;
        if($content=="bunkei"||$content=="reibun"||$content=="kaiwa"){
            return $this->getLesson("lessons_".$content,$content_of);
        }
        else if($content=="bunkei_francais"||$content=="reibun_francais"||$content=="kaiwa_francais"){
            return $this->getLesson($content,$content_of);
        }
        else if($content=="new_words"||$content=="lesson_info"){
            return $this->getLesson("fr_".$content,$content_of);
        }
    }
    public function AdminBackOfficeShow(Request $req){
        return DB::table($req->content)
        ->get();
    }
    public function ContentChange(Request $req){
        $object=$req->object;
        $id=$req->id;
        $lesson=$req->lesson;
        $head=$req->head;
        $content=$req->content;
        if($object=="bunkei"||$object=="reibun"){
            DB::table('lessons_'.$object)
            ->where('id',$id)
            ->update(['content'=>$content]);
            return 'Fait!';
        }
        else if($object=="bunkei_francais"||$object=="reibun_francais"){
            DB::table($object)
            ->where('id',$id)
            ->update(['content'=>$content]);
            return 'Fait!';
        }
        else if($object=="kaiwa"){
            DB::table("lessons_".$object)
            ->where('id',$id)
            ->update(['content'=>$content,'head'=>$head]);
            return 'Fait!';
        }
        else if($object=="kaiwa_francais"){
            DB::table($object)
            ->where('id',$id)
            ->update(['content'=>$content,'head'=>$head]);
            return 'Fait!';
        }
        else if($object=="new_words"){
            DB::table("fr_".$object)
            ->where('id',$id)
            ->update(['words'=>$content]);
            return 'Fait!';
        }
        else if($object=="lesson_info"){
            DB::table("fr_".$object)
            ->where('id',$id)
            ->update(['content'=>$content,'head'=>$head]);
            return 'Fait!';
        }
    }
    // Model methods
    protected function getLesson($lesson,$id){
        return DB::table($lesson)
        ->where('id_lesson',$id)
        ->get();
    }
    protected function checkLesson($lesson){
        $bunkei=DB::table('lessons_bunkei')
        ->where('id_lesson',$lesson)
        ->get();
        $reibun=DB::table('lessons_reibun')
        ->where('id_lesson',$lesson)
        ->get();
        $kaiwa=DB::table('lessons_kaiwa')
        ->where('id_lesson',$lesson)
        ->first();
        if(!$bunkei==null&&!$reibun==null&&!$kaiwa==null){
            DB::table('lessons')
            ->where('id',$lesson)
            ->update(['ready'=>1]);
        }
    }
}
