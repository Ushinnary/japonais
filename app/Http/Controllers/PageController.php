<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Auth;
class PageController extends Controller
{
    public function showLessons(){
        if(Auth::user()==null){
            return redirect('/');
        }
        $lessons=DB::table('lessons')
        ->where('ready',1)
        ->get();
        $lessons_done=DB::table('user_lessons_done')
        ->where('id_user',Auth::user()->id)
        ->get();
        $lesson_last=DB::table('user_lessons_done')
        ->where('id_user',Auth::user()->id)
        ->count();
        $lesson_last_done=DB::table('user_lessons_done')
        ->where('id_user',Auth::user()->id)
        ->orderBy('date','desc')
        ->first();
        return view('cours',['lessons'=>$lessons,'lessons_done'=>$lessons_done,'lessons_last'=>$lesson_last,'lesson_last_done'=>$lesson_last_done]);
    }
    public function showLesson($id){
        if(Auth::user()==null){
            return redirect('/');
        }
        $bunkei=DB::table('lessons_bunkei')
        ->where('id_lesson',$id)
        ->get();
        $reibun=DB::table('lessons_reibun')
        ->where('id_lesson',$id)
        ->get();
        $kaiwa=DB::table('lessons_kaiwa')
        ->where('id_lesson',$id)
        ->first();
        $passed=DB::table('user_lessons_done')
        ->where([['id_lesson',$id],['id_user',Auth::user()->id]])
        ->first();
        return view('lesson',['bunkei'=>$bunkei,'reibun'=>$reibun,'kaiwa'=>$kaiwa,'passed'=>$passed,'page'=>$id]);
    }
    public function showPosts(){
        if(Auth::user()==null){
            return redirect('/');
        }
        $post=DB::table('user_post')
        ->orderBy('date','desc')
        ->get();
        return view('posts',['post'=>$post]);
    }
    //
}
