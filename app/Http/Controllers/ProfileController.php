<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Auth;
use \App;
use \App\Chat;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
class ProfileController extends Controller
{
   public function  CommunChatJoin(){
    DB::insert("insert into chatusers (id_user,id_chat) values (?,?)",[Auth::user()->id,1]);
    }
   public function ShowChat(Request $req){
       $chat_id=$req->id;
       $chat_lm=$req->lm;
        return Chat::getCommonChatMessages($chat_lm);
    }
   public function  sendMessageChat(Request $req){
        Chat::sendMessage($req->message,$req->chat);
        return 'success';
    }
   public function  CommunChatCheck(){
        if(DB::table('chatusers')->where([['id_chat',1],['id_user',Auth::user()->id]])->first()!=null){
          return "Welcome";
        }else{
          return 'Join';
        }
    }
   public function  showProfile(){
        if(Auth::user()==null){
            return redirect('/');
        }
        $avatar=$this->getAvatar(Auth::user()->profileAvatar);
        $lessons_done=DB::table('user_lessons_done')
        ->where('id_user',Auth::user()->id)
        ->count();
        return view('profileMe',['lessons_done'=>$lessons_done]);
    }
   public function  LangChange(Request $req){
     if($req->id=="ru"||$req->id=="jp"||$req->id=="fr"){
       DB::table('users')
       ->where('id',Auth::user()->id)
       ->update(['language'=>$req->id]);
       return back();
      }else{
        return back();
      }
    }
   public function  showProfileFriend($id){
        if(Auth::user()==null){
            return redirect('/');
        }else if(Auth::user()->id==$id){
            return redirect('/');
        }
        $friend=DB::table('users')
        ->where('id',$id)
        ->get();
        if($friend->isEmpty()){
            return redirect('/');
        }
        foreach($friend as $user){
            $avatar=$this->getAvatar($user->profileAvatar);
        }
        $lessons_done=DB::table('user_lessons_done')
        ->where('id_user',$id)
        ->count();
        return view('profileFriend',['avatar'=>$avatar,'friend'=>$friend,'lessons_done'=>$lessons_done]);
    }
    public function background(){
        $color_left=substr(md5($friend->firstName),6,6);
        $color_right=substr(md5($friend->lastName),6,6);
    }
    public function showModerPage(){
        if(Auth::user()==null){
            return redirect('/');
        }
        if(Auth::user()->role=="moder"||Auth::user()->role=="admin"){
            return view('moderation');
        }else{
            return redirect()->back();
        }
    }
    public function showAdminPage(){
        if(Auth::user()==null){
            return redirect('/');
        }
        if(Auth::user()->role=="admin"){
            return view('administration');
        }else{
            return redirect()->back();
        }
    }
    public function avatarUpdate(Request $request){
        if ($request->hasFile('photo')) {
        // read image from temporary file
        $image=$request->photo;
        $extension = $image->extension();
        $img = Image::make($_FILES['photo']['tmp_name']);
        // resize image
        $img->fit(400, 400);
        $img->orientate();
        // save image
        $new_name=str_replace ('/', '',Hash::make(date("Y-m-d H:i:s")));
        $img->save('uploads/avatars/'.$new_name.".".$extension);
            // if(!Storage::disk('uploads')->putFileAs('/avatars',$image, Auth::user()->id.".".$extension)) {
            //     return false;
            // }
            DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['profileAvatar'=>'/uploads/avatars/'.$new_name.".".$extension]);
            return redirect('/profile');
        }
    }
    public function avatarDelete(){
        $toDelete=DB::table('users')
        ->where('id',Auth::user()->id)
        ->first();
        $reAvatar=str_replace("/uploads/avatars/",'',$toDelete->profileAvatar);
        echo public_path()."/uploads/avatars/".$reAvatar."<br />";
        if(file_exists(public_path()."/uploads/avatars/".$reAvatar)){
            echo "exist";
            unlink(public_path()."/uploads/avatars/".$reAvatar);
        }
            DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['profileAvatar'=>null]);
            return redirect('/profile');
    }
    public static function getAvatar($avatar){
        if($avatar==null){
            return"/images/default.jpg";
          }else if(file_exists(".".$avatar)){
            return $avatar;
          }
          else{
            return"/images/default.jpg";
          }
    }
    public function completeLesson(Request $req){
        DB::insert("insert into user_lessons_done (id_user,id_lesson,date) values (?,?,?)",[$req->id,$req->lesson,date("Y-m-d H:i:s")]);
        DB::table('users')
        ->where('id',$req->id)
        ->increment('levelxp',200);
    }
}
