<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \Auth;

class Chat extends Model
{
    public static function getCommonChatMessages($last_id){
        if($last_id!=0){
            return DB::table('chat_messages')
            ->join('users', 'users.id', '=', 'chat_messages.id_user')
            ->limit(10)
            ->where('chat_messages.id','>',$last_id)
            ->select('users.name','users.profileAvatar','chat_messages.date','chat_messages.message','chat_messages.id','chat_messages.id_user')
            ->get();
        }else{
            return DB::table('chat_messages')
            ->join('users', 'users.id', '=', 'chat_messages.id_user')
            ->where('chat_messages.id_chat',1)
            ->where([['chat_messages.id_chat',1],['chat_messages.id','>',$last_id]])
            ->orderBy('id','desc')
            ->limit(10)
            ->select('users.name','users.profileAvatar','chat_messages.date','chat_messages.message','chat_messages.id','chat_messages.id_user')
            ->get();
        }
    }
    public static function sendMessage($message,$chat){
        if($message!=null&&$message!=''){
            DB::insert('insert into chat_messages (id_user,id_chat,date,message) values(?,?,?,?)',[Auth::user()->id,$chat,date("Y-m-d H:i:s"),$message]);
         }
    }
}
