@extends('layouts.app')
@section('title')
Moderation
@endsection
@section('content')
<div class="container px-0">
<div class="card shadow mt-sm-4 mt-0 border-0 rounded">
    <h5 class="card-header bg-primary text-white text-center rounded-top">Actions</h5>
<div class="card-body">
<div class="row">
<div class="col-lg-4"><button class="btn btn-outline-primary mb-2" style="width:100%" id="add">Rajouter</button></div>
<div class="col-lg-4"><button class="btn btn-outline-warning mb-2" style="width:100%" id="modify">Editer</button></div>
<div class="col-lg-4"><button class="btn btn-outline-success mb-2" style="width:100%" id="post">Poster</button></div>
</div>
  </div>
</div>
<div class="card mt-sm-4 mt-1 shadow border-0 rounded">
  <div class="card-header text-center bg-primary text-white rounded-top">
   Moderation
  </div>
  <div class="card-body" id="form_output">
  <div class="text-left h1">Faut choisir l'action</div>
  </div>
  <div id="form_editing"></div>
</div>
</div>
@endsection
@section('bonus')
@endsection
@section('script')
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script>
$(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#add').click(function(){
        $('#form_editing').html('');
        $(this).addClass('active');
        $('#modify,#post').removeClass('active');
        $('#form_output').html('<form id="form_input"><div class="row"><div class="col-lg-6"><div class="form-group"><label>Rajouter ....</label><select name="add" class="form-control"><option value="bunkei">Bunkei</option><option value="reibun">Reibun</option><option value="kaiwa">Kaiwa</option><option value="new_words">Les mots de cour</option><option value="lesson_info">Les conseils</option><option value="bunkei_francais">Traduction Bunkei</option><option value="reibun_francais">Traduction Reibun</option><option value="kaiwa_francais">Traduction Kaiwa</option></select></div></div><div class="col-lg-6"><div class="form-group"><label>De...</label><select name="of" class="form-control" id="number"></select></div></div></div><div id="form_add"></div></form>');
        $.ajax({
                type: "POST",
                url: '/num_of',
                data: {
                    num_of:"all"
                    },
                success: function (data) {
                    for(var a in data){
                        if(data[a]["id"]==1){
                    $('#number').append('<option value="'+data[a]["id"]+'">'+data[a]["id"]+'er Cours</option>');
                        }else{
                    $('#number').append('<option value="'+data[a]["id"]+'">'+data[a]["id"]+'me Cours</option>');
                        }
                    }
                }
            });
            $('#form_add').html('<div class="form-group"><label>Contenu</label><textarea rows="3" class="form-control" name="text" required></textarea></div><button class="btn btn-primary" type="submit">Confirmer</button>');
            $('select[name="add"]').on('change',function(){
            if($(this).val()=="kaiwa"||$(this).val()=="kaiwa_francais"||$(this).val()=="lesson_info"){
            $('#form_add').html('<div class="form-group"><label>Titre</label><br /><input class="form-control" type="text" name="head"><br /><label>Contenu</label><textarea rows="3" class="form-control" name="text" required></textarea></div><button class="btn btn-primary" type="submit">Confirmer</button>');
            }else{
                $('#form_add').html('<div class="form-group"><label>Contenu</label><textarea rows="3" class="form-control" name="text" required></textarea></div><button class="btn btn-primary" type="submit">Confirmer</button>');
            }
    });
    $('#form_input').on('submit',function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
                url: '/add_content',
                data: {
                    object:$('select[name="add"]').val(),
                    lesson:$('select[name="of"]').val(),
                    head:$('input[name="head"]').val(),
                    content:$('textarea[name="text"]').val()
                    },
                success: function (data) {
                    $('#form_output').html(data);
                }
        });
    })
    });
    $('#modify').click(function(){
        $(this).addClass('active');
        $('#add,#post').removeClass('active');
        $('#form_output').html('<form id="form_modif"><div class="row"><div class="col-lg-6"><div class="form-group"><label>Modifier ....</label><select name="modify" class="form-control"><option value="bunkei">Bunkei</option><option value="reibun">Reibun</option><option value="kaiwa">Kaiwa</option><option value="new_words">Les mots de cour</option><option value="lesson_info">Les conseils</option><option value="bunkei_francais">Traduction Bunkei</option><option value="reibun_francais">Traduction Reibun</option><option value="kaiwa_francais">Traduction Kaiwa</option></select></div></div><div class="col-lg-6"><div class="form-group"><label>De...</label><select name="of" class="form-control" id="number"></select></div></div></div><div id="form_add"></div></form>');
        $.ajax({
                type: "POST",
                url: '/num_of',
                data: {
                    num_of:$('select[name="modify"]').val()
                    },
                success: function (data) {
                    console.log(data);
                    for(var a in data){
                        if(data[a]["id_lesson"]==1){
                    $('#number').append('<option value="'+data[a]["id_lesson"]+'">'+data[a]["id_lesson"]+'er Cours</option>');
                        }else{
                    $('#number').append('<option value="'+data[a]["id_lesson"]+'">'+data[a]["id_lesson"]+'me Cours</option>');
                        }
                    }
                    getContent();
                }
            });
            $('select[name="modify"]').on('change',function(){
                $.ajax({
                type: "POST",
                url: '/num_of',
                data: {
                    num_of:$('select[name="modify"]').val()
                    },
                success: function (data) {
                    $('#number').html('');
                    for(var a in data){
                        if(data[a]["id_lesson"]==1){
                    $('#number').append('<option value="'+data[a]["id_lesson"]+'">'+data[a]["id_lesson"]+'er Cour</option>');
                        }else{
                    $('#number').append('<option value="'+data[a]["id_lesson"]+'">'+data[a]["id_lesson"]+'me Cour</option>');
                        }
                    }
                    getContent();
                }
            });
            })
                $('select[name="of"]').on('change',getContent);
            function getContent(){
                $.ajax({
                type: "POST",
                url: '/content_get',
                data: {
                    content:$('select[name="modify"]').val(),
                    content_of:$('select[name="of"]').val()
                    },
                success: function (data) {
                    content=$('select[name="modify"]').val();
                    $('#form_editing').html('');
                        for(var a in data){
                   if(content=="bunkei"||content=="reibun"||content=="bunkei_francais"||content=="reibun_francais"){
                       $('#form_editing').append('<div class="card-body"><form><div class="form-group"><input type="hidden" value="'+$('select[name="modify"]').val()+'" name="modify"><input type="hidden" value="'+data[a]['id']+'" name="id"><input type="hidden" value="'+$('select[name="of"]').val()+'" name="of"><label>Contenu</label><textarea rows="3" class="form-control" name="text" required>'+data[a]["content"]+'</textarea></div><button class="btn btn-primary" type="submit">Confirmer Modifications</button></form></div>');
                    }
                    else if(content=="kaiwa"||content=="kaiwa_francais"||content=="lesson_info"){
                        $('#form_editing').append('<div class="card-body"><form><div class="form-group"><label>Titre</label><br /><input type="hidden" value="'+$('select[name="modify"]').val()+'" name="modify"><input type="hidden" value="'+data[a]['id']+'" name="id"><input type="hidden" value="'+$('select[name="of"]').val()+'" name="of"><input class="form-control" type="text" name="head" value="'+data[a]["head"]+'"><br /><label>Contenu</label><textarea rows="3" class="form-control" name="text" required>'+data[a]["content"]+'</textarea></div><button class="btn btn-primary" type="submit">Confirmer Modifications</button></form></div>');
                }
                    else if(content=="new_words"){
                        $('#form_editing').append('<div class="card-body"><form><div class="form-group"><input type="hidden" value="'+$('select[name="modify"]').val()+'" name="modify"><input type="hidden" value="'+data[a]['id']+'" name="id"><input type="hidden" value="'+$('select[name="of"]').val()+'" name="of"><label>Contenu</label><textarea rows="3" class="form-control" name="text" required>'+data[a]["words"]+'</textarea></div><button class="btn btn-primary" type="submit">Confirmer Modifications</button></form></div>');
                }
            }
            $('form').on('submit',function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
                url: '/change_content',
                data: {
                    object:$(this).find('input[name="modify"]').val(),
                    id:$(this).find('input[name="id"]').val(),
                    lesson:$(this).find('input[name="of"]').val(),
                    head:$(this).find('input[name="head"]').val(),
                    content:$(this).find('textarea[name="text"]').val()
                    },
                success: function (data) {
                    $('#form_output').html(data);
                    $('#form_editing').html('');
                }
        });
    })
                    }
            });
            }
        $('select[name="of"]').on('change',getContent);
    });
    $('#post').click(function(){
        $('#form_editing').html('');
        $(this).addClass('active');
        $('#add,#modify').removeClass('active');
        $('#form_output').html('<form id="form_post" enctype="multipart/form-data"><label>Titre</label><input type="text" name="title" class="form-control mb-2"><label>Contenu de post <small>(Au moins 20 caracteres)</small></label><textarea id="text" name="text" pattern=".{5,}" required></textarea><input type="file" class="form-control mt-2" name="photo"><input class="btn btn-outline-success mt-2" type="submit" value="Poster"></form>');
        CKEDITOR.replace('text');
        // <div class="py-4" id="chiffres_saisi">Caracteres saisi : 0</div>
        // $('#text').change(caracteres_saisi(this.value));
        // function caracteres_saisi(a){
        //     $(this).html('Caracteres saisi : ',a.length);
        //     console.log(a.length);
        // }
        $('#form_post').on('submit',function(e){
            e.preventDefault();
        if($('textarea[name="text"]').val().length>=20){
            var formData=new FormData(this);
            $.ajax({
                type: "POST",
                url: '/add_content',
                data:{
                    object:"post",
                    title:$(this).find('input[name="title"]').val(),
                    text:$(this).find('textarea[name="text"]').val(),
                    photo:$(this).find('input[type="file"]').val()
                },
                success:function(data){
                }
            });
                    $('#form_output').html('Fait!');
        }
        });
    });
});
</script>
@endsection