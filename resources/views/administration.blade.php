@extends('layouts.app')
@section('title')
Moderation
@endsection
@section('content')
<div class="container-fluid px-0 mx-0">
<div class="row">
<div class="col-lg-3 px-0">
  <button id="users" class="btn btn-primary" style="width:100%">Utilisateurs</button>
  <button id="chat" class="btn btn-warning" style="width:100%">Chats</button>
</div>
<div class="col-lg-9 px-0">
<div class="text-dark bg-white">
<div id="output"></div>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
		$('#users,#chat').click(function(){
			var content=$(this).attr('id');
			console.log(content);
			$('#output').html('');
			$.ajax({
				type:'post',
				url:'/admin_show',
				data:{
					content:content
				},
				success:function(data){
					console.log(data);
					if(content=="users"){
					for(var a in data){
					$('#output').append("<div class='p-4'>ID : "+data[a]['id']+"<br />Name : "+data[a]['name']+"<br />Email : "+data[a]['email']+"<br />Role : "+data[a]['role']+"<br />Level XP : "+data[a]['levelxp']+"</div>");
					}
					}
				}
			});
		});
});
</script>
@endsection