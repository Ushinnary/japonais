<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Japonais</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
    <style>
        .bg-img {
            position: absolute;
left: 0;
width: 100%;
height: 100%;
z-index: -1;
background: url('../images/bg.png') no-repeat center;
background-size: cover;
        }

        #bg {
            background: rgba(0, 0, 0, .6);
        }
        #reg,#log{
            cursor:pointer;
        }

        @media only screen and (min-device-width: 320px) and (max-device-width: 480px) and (orientation: portrait) {
            .bg-img {
                background: url('../images/mobile.png') no-repeat center;
                background-size: cover;
            }
            .display-2 {
                font-size: 4.5rem;
            }
            .hidden-sm {
                display: none !important;
            }
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 order-lg-1 order-2 d-flex flex-column align-items-center justify-content-center bg-white text-primary" style="height:100vh;">
                <h1 class="mb-2">Happy Jappy</h1>
                <div class="card border-0" style="width:100%;">
                    <div class="card-header shadow-none border-0 h1 bg-white text-primary" id="header">Se connecter</div>
                    <div class="card-body p-2">
                        <div class="card-text" id="form">
                            <form method="post" action="/user_login">
                                @csrf
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label>Mot de passe</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Confirmer</button>
                                </div>
                            </form>
                            <div class="text-center mt-4">
                                Je n'ai pas d'account
                                <u id="reg">S'inscrire</u>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 d-flex align-items-center" style="height:100vh;background:rgba(0,0,0,.5);">
            <div class="bg-img"></div>
                <div class="jumbotron jumbotron-fluid text-center text-white" style="width:100%;background: transparent;">
                    <div class="container">
                        <h1 class="display-2 font-weight-bold">Happy Jappy</h1>
                        <p class="h2">Apprenez le japonais sans vous prendre la tête</p>
                    </div>
                </div>
            </div>
        </div>
    <script>
        $(document).ready(function () {
            $('#reg').click(reg);
                function reg(){
                $('#header').html("S'inscrire");
                $('#form').html("<form method='post' action='/user_register'><input type='hidden' name='_token' value='{{Session::token()}}'><div class='form-group'><label>Email address</label><input type='email' class='form-control' name='email' placeholder='exemple@ex.com'></div><div class='form-group'><label>Nom d'utilisateur</label><input type='text' class='form-control' name='name'></div><div class='form-group'><label>Mot de passe</label><input type='password' class='form-control' name='password' placeholder='Password'></div><div class='text-right'><button type='submit' class='btn btn-primary'>Confrimer</button></div></form><div class='text-center mt-4'>J'ai deja un account <u id='log'>Se connecter</u></div>");
                $('#log').click(log);
                }
            $('#log').click(log);
                function log(){
                $('#header').html("Se connecter");
                $('#form').html("<form method='post' action='/user_login'><input type='hidden' name='_token' value='{{Session::token()}}'><div class='form-group'><label>Email address</label><input type='email' class='form-control' name='email' required></div><div class='form-group'><label>Mot de passe</label><input type='password' class='form-control' name='password'></div><div class='text-right'><button type='submit' class='btn btn-primary'>Confirmer</button></div></form><div class='text-center mt-4'>Je n'ai pas d'account <u id='reg'>S'inscrire</u></div>");
                $('#reg').click(reg);
                }
        });
    </script>
</body>

</html>