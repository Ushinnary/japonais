<?php 
$avatarMe=\App\Http\Controllers\ProfileController::getAvatar(Auth::user()->profileAvatar);
App::setLocale(Auth::user()->language);
?>
@extends('layouts.app') @section('title') {{Auth::user()->name}} @endsection @section('css')
<style>
    label {
        cursor: pointer;
    }

    #upload-photo {
        opacity: 0;
        position: absolute;
        z-index: -1;
    }
</style>
@endsection 
@section('content')
<div class="container">
<div class="row">
<div class="col-lg-4 px-0 px-sm-2">
<div class="card my-lg-4 my-0 border-0 rounded">
          <div class="col-lg-8 col-8 mx-auto mt-2">
            <div class="img_profile" style="background:url('{{$avatarMe}}') no-repeat center;background-size:cover;border-radius:50%;border: 3px solid white;box-shadow: 0 0 3px white;">
            </div>
          </div>
          <div class="card-body">
            <h4 class="card-title text-center">{{Auth::user()->name}}</h4>
            @if(Auth::user()->role=='admin')
            <p class="card-text text-center">
            <?=__('profile.admin');?>
            </p>
            @elseif(Auth::user()->role=='moder')
            <p class="card-text text-center">
            <?=__('profile.moder');?>
            </p>
            @endif
            <p class="card-text text-center">
            @if(Auth::user()->levelxp<2000)
            {{Auth::user()->levelxp}}/2000 XP (<?=__('profile.novice');?>)
            @else
            {{Auth::user()->levelxp%2000}}/2000 XP (<?= __('profile.level',['lvl'=>floor(Auth::user()->levelxp/2000)]);?>)
            @endif
            </p>
            <div class="row">
              <div class="col-lg-6 col-6 text-right" style="font-size: 0.75rem;border-right: 1px solid;">
              <?=__('profile.lessonscompleted');?>
                <br />
                <span>@if($lessons_done!=null)
				{{$lessons_done}}
				@else
				0
				@endif</span>
              </div>
              <div class="col-lg-6 col-6 text-left" style="font-size: 0.75rem;">
              <?=__('profile.exercisescompleted');?>
                <br />
                <span>Pas prêt</span>
              </div>
            </div>
          </div>
        </div>
</div>
<div class="col-lg-8 px-0 px-sm-2">
<div class="card mt-sm-4 mt-1  border-0 rounded">
    <div class="card-header bg-primary text-white text-center rounded-top"><?=__('profile.avatarchange');?></div>
    <div class="card-body">
        <form action="/profile_image_update" method="post" enctype="multipart/form-data">
            @csrf
            <label for="upload-photo" class="btn btn-outline-primary" style="width:100%;"><?=__('profile.select');?>...</label>
            <input type="file" name="photo" id="upload-photo" required>
            <input type="submit" class="btn btn-outline-primary" style="width:100%;" value="<?=__('profile.confirm');?>">
        </form>
        <form action="/profile_image_delete" method="post">
            @csrf
            <input type="submit" class="btn btn-outline-danger mt-2" style="width:100%" value="<?=__('profile.deleteavatar');?>">
        </form>
    </div>
</div>
</div>
</div>
</div>
@endsection 
@section('script')
<script>
$(document).ready(function(){
    $('.img_profile').height($('.img_profile').width());
})
</script>
@endsection