@extends('layouts.app') @section('title') Cours {{$page}} @endsection @section('content')
<?php App::setLocale(Auth::user()->language);?>
<div class="actual_lesson" id="{{$page}}"></div>
<div class="container px-0">
	<div class="card mt-sm-4 mt-0  border-0 rounded">
		<div class="card-header bg-primary text-center text-white h1 rounded-top">
			<ruby>第
				<rt>だい</rt>
			</ruby> {{$page}}
			<ruby>課
				<rt>か</rt>
			</ruby>
		</div>
		<div class="card-body h2">
			<div class="text-danger mt-2">
				<ruby>文
					<rt>ぶん</rt>
				</ruby>
				<ruby>型
					<rt>けい</rt>
				</ruby>
			</div>
			<ol class="list-group list-group-flash my-2 px-4">
				@foreach($bunkei as $preview_a)
				<li class="h4">{!!$preview_a->content!!}</li>
				@endforeach
			</ol>
			@if($page<=9) 
			<audio controls style="width:100%">
				<source src="../audio/0{{$page}} - 02 - Bunkei.mp3" type="audio/mp3"> Your browser does not support the audio element.
				</audio>
				@elseif($page>=10)
				<audio controls style="width:100%">
					<source src="../audio/{{$page}} - 02 - Bunkei.mp3" type="audio/mp3"> Your browser does not support the audio element.
				</audio>
				@endif
				<div class="text-danger mt-2">
					<ruby>例
						<rt>れい</rt>
					</ruby>
					<ruby>文
						<rt>ぶん</rt>
					</ruby>
				</div>
				<ol class="list-group list-group-flash my-2 px-4">
					@foreach($reibun as $preview_b)
					<li class="h4">{!!$preview_b->content!!}</li>
					@endforeach
				</ol>
				@if($page<=9) 
				<audio controls style="width:100%">
					<source src="../audio/0{{$page}} - 03 - Reibun.mp3" type="audio/mp3"> Your browser does not support the audio element.
					</audio>
					@elseif($page>=10)
					<audio controls style="width:100%">
						<source src="../audio/{{$page}} - 03 - Reibun.mp3" type="audio/mp3"> Your browser does not support the audio element.
					</audio>
					@endif 
					<div class="text-danger mt-2">
						<ruby>会
							<rt>かい</rt>
						</ruby>
						<ruby>話
							<rt>わ</rt>
						</ruby>
					</div>
					<div class="text-center h3">
						{!!$kaiwa->head!!}
					</div>
					<div class="h5">{!!$kaiwa->content!!}</div>
					@if($page<=2) 
					<audio controls style="width:100%">
						<source src="../audio/0{{$page}} - 05 - Kaiwa.mp3" type="audio/mp3"> Your browser does not support the audio element.
					</audio>
						@elseif($page<=9)
						<audio controls style="width:100%">
						<source src="../audio/0{{$page}} - 04 - Kaiwa.mp3" type="audio/mp3"> Your browser does not support the audio element.
					</audio>
						@elseif($page>=10)
					<audio controls style="width:100%">
						<source src="../audio/{{$page}} - 04 - Kaiwa.mp3" type="audio/mp3"> Your browser does not support the audio element.
					</audio>
						@endif
		</div>
	</div>
	<div class="card mt-sm-4 mt-1 shadow border-0">
		<div class="card-header bg-primary text-white">
		<?=__('lessons.lessonstatus');?>
		</div>
		<div class="card-body" id="lesson_complete_status">
			@if($passed!=null)
			<div class="text-primary">
			<?=__('lessons.lessondone');?>
			</div>
			@else
			<div class="text-primary mb-2">
			<?=__('lessons.lessonnotdone');?>
			</div>
			<button class="btn btn-outline-primary" style="width:100%;" id="lesson_completed"><?=__('lessons.lessondone');?></button>
			@endif
		</div>
	</div>
</div>
<div class="container text-center">
    <a class="btn btn-primary px-2 mb-2" href="/cours"><?=__('lessons.back');?></a>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	$('#lesson_completed').click(function(){
		$.ajax({
			type: "POST",
			url: '/lesson_complete',
			data: {
			id:$('.session_user').attr('id'),
			lesson:$('.actual_lesson').attr('id')
			},
			success: function (data) {
				$('#lesson_complete_status').html('<div class="text-primary mb-2">Vous avez completé ce cours . +200xp</div>');
			}
		});
	})
})
</script>
@endsection