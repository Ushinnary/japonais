@extends('layouts.app')
@section('title')
Les Cours
@endsection
@section('content')
<?php App::setLocale(Auth::user()->language);?>
<div class="container  mx-0 px-0 mx-sm-auto">
<div class="card mt-sm-4 mt-0  border-0 rounded">
        <h1 class="card-header text-center display-4 bg-primary text-white rounded-top"><?=__('lessons.minnanonihongo');?></h1>
				<div class="row mx-0">
				<div class="col-lg-6 mt-2">
				<div class="card border-0 bg-primary text-white mx-1 mx-sm-0">
      <div class="card-body text-center">
			<?=__('lessons.numoflessonscompleted');?>
				<p class="card-text text-center">
				{{$lessons_last}}
				</p>
      </div>
    </div>
				</div>
				<div class="col-lg-6 mt-2">
				<div class="card border-0 bg-primary text-white mx-1 mx-sm-0">
      <div class="card-body text-center">
			<?=__('lessons.lastlessoncompleted');?>
				<p class="card-text text-center">
				@if($lesson_last_done!=null)
				{{$lesson_last_done->id_lesson}}
				@else
				0
				@endif
				</p>
      </div>
    </div>
    </div>
				</div>
				<div class="row mx-0">
				@foreach($lessons as $cours)
				<div class="col-lg-3 mt-2 mx-1 mx-sm-0">
				<?php 
				$random=rand(1,6);
				if($random==1){
					$card_bg='bg-primary text-white';
				}
				else if($random==2){
					$card_bg='bg-secondary text-white';
				}
				else if($random==3){
					$card_bg='bg-info text-white';
				}
				else if($random==4){
					$card_bg='bg-success text-white';
				}
				else if($random==5){
					$card_bg='bg-warning text-white';
				}
				else if($random==6){
					$card_bg='bg-danger text-white';
				}
				?>
				<a href="/cours/{{$cours->id}}"><div class="card rounded shadow border-0 {{$card_bg}}">
      <div class="card-body">
        <h1 class="card-title text-center"><?=__('lessons.lesson');?> : {{$cours->id}}</h1>
        <p class="card-text text-center"><?=__('lessons.lessonstatus');?> : 
				<?php 
				if(!$lessons_done->isEmpty()){
					foreach($lessons_done as $cours_fini){
						if($cours_fini->id_lesson==$cours->id){
							$lesson_state=__('lessons.lessondone');
							break;
						}else{
							$lesson_state=__('lessons.lessonnotdone');
						}
					}echo $lesson_state;
				}else{
					echo __('lessons.lessonnotdone');
				}
				?>
				</p>
      </div>
    </div>
				</div>
				</a>
		@endforeach
      </div>
      </div>
@endsection