@extends('layouts.app') @section('title') Les Posts @endsection 
@section('css')
<style>
ul{
	padding: 0 1rem;
}
</style>
@endsection
@section('content')
<div class="container px-0">
			@foreach($post as $posts)
			<div class="card shadow bg-white text-dark mx-1 rounded mb-sm-4 mb-4 mt-sm-4 mt-0">
				@if(file_exists("uploads/post/$posts->id.jpeg"))
				<img class="card-img-top" src="/uploads/post/{{$posts->id}}.jpeg"> @endif
				<div class="card-body">
					@if($posts->title!=null)
					<div class="card-title text-center h3">{{$posts->title}} {{$posts->id}}</div>
					@endif {!!$posts->content!!}
				</div>
				<?php 
					$author=\App\Profile::getUser($posts->id_author);
					?>
						<?php 
						$avatar=\App\Http\Controllers\ProfileController::getAvatar($author->profileAvatar);
						?>
				<div class="card-footer py-0">
						<a href="/profile/{{$posts->id_author}}" class="d-flex justify-content-start align-items-center">
						<div style="width:2rem;height:2rem;background:url('{{$avatar}}') no-repeat center;background-size:cover;border-radius:50%; border:1px solid white;" ></div> <span class="px-2">{{$author->name}}</span>
						</a>
					<small class="text-muted text-left">
						{{$posts->date}}
					</small>
					</div>
				</div>
			@endforeach
					</div>
@endsection