@extends('layouts.app') @foreach($friend as $user) @endforeach @section('title') {{$user->name}} @endsection @section('css')
<style>
  label {
    cursor: pointer;
  }

  #upload-photo {
    opacity: 0;
    position: absolute;
    z-index: -1;
  }
  .session_friend{
    display:none;
  }
</style>
@endsection @section('content')
<?php App::setLocale(Auth::user()->language);?>
<div class="session_friend" id="{{$user->id}}"></div>
<div class="container">
  <div class="row">
    <div class="col-lg-4 px-0 px-sm-2">
      <div class="card my-lg-4 my-0 border-0 rounded">
        <div class="col-lg-8 col-8 mx-auto mt-2">
          <div class="img_profile" style="background:url('{{$avatar}}') no-repeat center;background-size:cover;border-radius:50%;border:3px solid white;"></div>
        </div>
        <div class="card-body">
          <h5 class="card-title text-center">{{$user->name}}</h5>
          @if($user->role=='admin')
            <p class="card-text text-center">
            <?=__('profile.admin');?>
            </p>
            @elseif($user->role=='moder')
            <p class="card-text text-center">
            <?=__('profile.moder');?>
            </p>
            @endif
          <p class="card-text text-center">
          @if($user->levelxp<2000)
            {{$user->levelxp}} XP (1er Niveau)
            @else
            {{$user->levelxp%2000}}/2000 XP (<?= __('profile.level',['lvl'=>floor(Auth::user()->levelxp/2000)]);?>)
            @endif
          </p>
          <div class="row">
            <div class="col-lg-6 col-6 text-right" style="border-right: 1px solid;">
            <?=__('profile.lessonscompleted');?>
              <br />
              <small>
              @if($lessons_done!=null)
				{{$lessons_done}}
				@else
				0
				@endif
        </small>
            </div>
            <div class="col-lg-6 col-6 text-left">
            <?=__('profile.exercisescompleted');?>
              <br />
              <small>Pas prêt</small>
            </div>
          </div>
          <div class="text-center" id="friend_action"></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection @section('script')
<script>
  $(document).ready(function () {
    $('.img_profile').height($('.img_profile').width());
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
		// $.ajax({
		// 	type: "POST",
		// 	url: '/friend_state_check',
		// 	data: {
		// 	id:$('.session_friend').attr('id'),
		// 	},
		// 	success: function (data) {
		// 		$('#lesson_complete_status').html('<div class="text-primary mb-2">Vous avez completé ce cours . +200xp</div>');
		// 	}
		// });
  })
</script>
@endsection