<?php 
$avatarMe=\App\Http\Controllers\ProfileController::getAvatar(Auth::user()->profileAvatar);
App::setLocale(Auth::user()->language);
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/layout.css') }}" rel="stylesheet">
  <script src="{{asset('js/jquery.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
    crossorigin="anonymous"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <title>@yield('title')</title>
  @yield('css')
</head>

<body>
  <div class="bg-img"></div>
  <div class="bg-img-glass"></div>
  <div class="session_user" id="{{Auth::user()->id}}"></div>
  <div id="chat_toggler" class="chat bg-primary text-white" data-toggle="modal" data-target="#chat"><?php echo __('navigation.chat');?></div>
  <div class="modal fade " id="chat" tabindex="-1" role="dialog" aria-labelledby="chat" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary text-white">
          <h5 class="modal-title " id="exampleModalCenterTitle">
            <?=__('navigation.common');?>
          </h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span id="chat_close" aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body p-0">
          <ul id="area_chat" style="display:flex;flex-direction:column;height: 60vh;overflow-y: auto;margin:0;">
          <button class="btn btn-outline-primary" id="area_join" style="height: 100%;width: 100%;text-align: center;font-size: 3rem;"><?php echo __("chat.join");?></button>
          </ul>
        </div>
        <div class="modal-footer text-right" id="area_typing">
        <form style="width:100%" class="text-right" id="chat_form"><button type="submit" class="btn btn-primary mb-2"><?php echo __('chat.send')?></button><div class="form-group"><textarea class="form-control" id="textarea_message" rows="1"></textarea></div></form>
        </div>
      </div>
    </div>
  </div>
  <div class="navigation " data-toggle="modal" data-target="#exampleModalCenter">
    <div class="img_profile_me" style="background:url('{{$avatarMe}}') no-repeat center;background-size:cover;border-radius:50%;width:100%;height:100%;">
    </div>
  </div>
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary text-white">
          <h5 class="modal-title " id="exampleModalCenterTitle">
            <?=__('navigation.navigation');?>
          </h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul class="list-group list-group-flush text-dark">
            <li class="list-group-item">
              <a href="/profile" class="text-danger">
                <?=__('navigation.profile');?>
              </a>
            </li>
            <li class="list-group-item">
              <a href="/cours">
                <?=__('navigation.lessons');?>
              </a>
            </li>
            <li class="list-group-item">
              <?=__('navigation.exercises');?>
            </li>
            <li class="list-group-item">
              <a href="/posts">
                <?=__('navigation.posts');?>
              </a>
            </li>
            <li class="list-group-item">
              <?=__('navigation.messages');?>
            </li>
            <li class="list-group-item">
              <?=__('navigation.friends');?>
            </li>
            @if(Auth::user()->role=="admin")
            <li class="list-group-item">
              <a href="/admin">
                <?=__('navigation.administration');?>
              </a>
            </li>
            <li class="list-group-item">
              <a href="/moderation">
                <?=__('navigation.moderation');?>
              </a>
            </li>
            @elseif(Auth::user()->role=="moder")
            <li class="list-group-item">
              <a href="/moderation" class="text-success">
                <?=__('navigation.moderation');?>
              </a>
            </li>
            @endif
            <li class="list-group-item">
              <a href="/logout">
                <?=__('navigation.exit');?>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  @yield('content') @yield('script')
  <script src="../js/chat.js"></script>
</body>

</html>