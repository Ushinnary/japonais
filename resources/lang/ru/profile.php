<?php 
return [
	'admin'=>'Администратор',
	'moder'=>'Модератор',
	'novice'=>'Новичок',
	'level'=>':lvl й уровень',
	'lessonscompleted'=>'Уроков выполнено',
	'exercisescompleted'=>'Упражнений выполнено',
	'avatarchange'=>'Сменить фотографию профиля',
	'select'=>'Выбрать',
	'confirm'=>'Подтвердить',
	'deleteavatar'=>'Удалить фотографию профиля',
];