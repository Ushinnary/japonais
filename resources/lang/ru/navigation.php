<?php 
return [
	'navigation'=>'Навигация',
	'profile'=>'Профиль',
	'lessons'=>'Уроки',
	'exercises'=>'Упражнения',
	'posts'=>'Посты',
	'messages'=>'Сообщения',
	'friends'=>'Друзья',
	'administration'=>'Администрирование',
	'moderation'=>'Модерирование',
  'exit'=>'Выйти',
  'chat'=>'Чат',
  'common'=>"Общий"
];