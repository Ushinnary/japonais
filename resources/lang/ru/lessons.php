<?php 
return [
	'minnanonihongo'=>'МиннаНоНихонго',
	'numoflessonscompleted'=>'Уроков закончено : ',
	'lastlessoncompleted'=>'Последний пройденный урок : ',
	'lesson'=>'Урок',
	'lessonstatus'=>'Состояние',
	'lessondone'=>'Выполнен',
	'lessonnotdone'=>'Не выполнен',
	'back'=>'Назад'
];