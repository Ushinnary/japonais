<?php 
return [
	'navigation'=>'Navigation',
	'profile'=>'Profil',
	'lessons'=>'Les Cours',
	'exercises'=>'Les Exercices',
	'posts'=>'Les Posts',
	'messages'=>'Les Messages',
	'friends'=>'Les Amis',
	'administration'=>'Administration',
	'moderation'=>'Moderation',
  'exit'=>'Deconnexion',
  'chat'=>'Chat',
  'common'=>'Commun'
];