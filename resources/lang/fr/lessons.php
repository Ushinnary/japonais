<?php 
return [
	'minnanonihongo'=>'Minna No Nihongo',
	'numoflessonscompleted'=>'Nombre des courses completé : ',
	'lastlessoncompleted'=>'Le dernier cours completé : ',
	'lesson'=>'Cours',
	'lessonstatus'=>'Statut',
	'lessondone'=>'Completé',
	'lessonnotdone'=>'N\'est Completé',
	'back'=>'Revenir à la liste des courses'
];