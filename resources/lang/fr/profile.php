<?php 
return [
	'admin'=>'Administrateur',
	'moder'=>'Moderateur',
	'novice'=>'Amateur',
	'level'=>':lvl me niveau',
	'lessonscompleted'=>'Les Courses Completé',
	'exercisescompleted'=>'Les Exercices Completé',
	'avatarchange'=>'Changer l\'image de profil',
	'select'=>'Choisir l\'image de profil',
	'confirm'=>'Confirmer',
	'deleteavatar'=>'Supprimer l\'image de profil',
];