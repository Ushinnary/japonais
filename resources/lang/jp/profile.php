<?php 
return [
	'admin'=>'管理者',
	'moder'=>'モデレータ',
	'novice'=>'初心者',
	'level'=>'第 :lvl レベル',
	'lessonscompleted'=>'レッスン完了',
	'exercisescompleted'=>'演習が完了しました',
	'avatarchange'=>'プロフィール写真を変更する',
	'select'=>'選択',
	'confirm'=>'確認',
	'deleteavatar'=>'アバターを削除',
];