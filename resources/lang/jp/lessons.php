<?php 
return [
	'minnanonihongo'=>'みんなの日本語',
	'numoflessonscompleted'=>'完了したレッスン : ',
	'lastlessoncompleted'=>'最後に完了したレッスン : ',
	'lesson'=>'レッスン',
	'lessonstatus'=>'レッスンのステータス',
	'lessondone'=>'レッスンを終えた',
	'lessonnotdone'=>'レッスンをしていない',
	'back'=>'戻る'
];