<?php 
return [
	'navigation'=>'ナビゲーション',
	'profile'=>'プロフィール',
	'lessons'=>'レッスン',
	'exercises'=>'演習',
	'posts'=>'ニュース',
	'messages'=>'メッセージ',
	'friends'=>'友達',
	'administration'=>'投与',
	'moderation'=>'節度',
  'exit'=>'切断する',
  'chat'=>'チャット',
  'common'=>'一般'
];