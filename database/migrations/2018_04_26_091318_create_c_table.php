<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_lesson');
            $table->integer('number');
            $table->string('content');
            $table->string('rei')->nullable();
            $table->string('input');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c');
    }
}
